// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// tslint:disable
/**
 * eGesetzgebung Platform
 * This is the rest interface offered by the eGesetzgebung Platform backend.  Please provide __JW Access Token__ using button \'Authorize\' to access protected endpoints. Use the following instructions to get token:  1. on CMD/Bash/Terminal: ```curl -d \"client_id=user-management\" -d \"client_secret=INSERT_IAM_SECRET\" -d \"grant_type=password&username=INSERT_USER_EMAIL&password=INSERT_USER_PASSWORD\" \"INSERT_IAM_URL/realms/bund/protocol/openid-connect/token\"``` 2. copy access_token (without quotes, you can use jq or other tools to extract this value automatically) 3. insert value into \'JWToken\' field under \'Authorize\' and press \'Authorize\' 4. now you should have access to user protected endpoint  __FYI:__ token expires after 60 seconds. On non-production systems only a warning is displayed so you can work smoothly, but on production systems you have to frequently provide a new token.
 *
 * The version of the OpenAPI document: 9.0.3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import type {
    ZeitplanungselementHinweisType,
    ZeitplanungsvorlagenBauplan,
} from './';

/**
 * @export
 * @interface ZeitplanungselementEntityDTO
 */
export interface ZeitplanungselementEntityDTO {
    /**
     * @type {boolean}
     * @memberof ZeitplanungselementEntityDTO
     */
    readonly termin: boolean;
    /**
     * @type {boolean}
     * @memberof ZeitplanungselementEntityDTO
     */
    wichtig: boolean;
    /**
     * @type {ZeitplanungsvorlagenBauplan}
     * @memberof ZeitplanungselementEntityDTO
     */
    bauplan?: ZeitplanungsvorlagenBauplan;
    /**
     * @type {string}
     * @memberof ZeitplanungselementEntityDTO
     */
    titel: string;
    /**
     * @type {string}
     * @memberof ZeitplanungselementEntityDTO
     */
    uebergeordneteElementeId?: string;
    /**
     * @type {Array<string>}
     * @memberof ZeitplanungselementEntityDTO
     */
    vorherigeElementeIds: Array<string>;
    /**
     * @type {string}
     * @memberof ZeitplanungselementEntityDTO
     */
    kommentar?: string;
    /**
     * Sollen die nachfolgenden Elemente verschoben und übergeordnete Phasen angepasst werden?
     * @type {boolean}
     * @memberof ZeitplanungselementEntityDTO
     */
    zeitplanungAnpassen: boolean;
    /**
     * @type {Array<ZeitplanungselementHinweisType>}
     * @memberof ZeitplanungselementEntityDTO
     */
    hinweise: Array<ZeitplanungselementHinweisType>;
    /**
     * Darf das Element nur vom Ersteller bearbeitet werden?
     * @type {boolean}
     * @memberof ZeitplanungselementEntityDTO
     */
    gesperrt: boolean;
    /**
     * @type {string}
     * @memberof ZeitplanungselementEntityDTO
     */
    beginn?: string;
    /**
     * @type {string}
     * @memberof ZeitplanungselementEntityDTO
     */
    ende?: string;
    /**
     * @type {string}
     * @memberof ZeitplanungselementEntityDTO
     */
    datum?: string;
}


