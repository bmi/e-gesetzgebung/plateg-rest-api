// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// tslint:disable
/**
 * eGesetzgebung Platform
 * This is the rest interface offered by the eGesetzgebung Platform backend.  Please provide __JW Access Token__ using button \'Authorize\' to access protected endpoints. Use the following instructions to get token:  1. on CMD/Bash/Terminal: ```curl -d \"client_id=user-management\" -d \"client_secret=INSERT_IAM_SECRET\" -d \"grant_type=password&username=INSERT_USER_EMAIL&password=INSERT_USER_PASSWORD\" \"INSERT_IAM_URL/realms/bund/protocol/openid-connect/token\"``` 2. copy access_token (without quotes, you can use jq or other tools to extract this value automatically) 3. insert value into \'JWToken\' field under \'Authorize\' and press \'Authorize\' 4. now you should have access to user protected endpoint  __FYI:__ token expires after 60 seconds. On non-production systems only a warning is displayed so you can work smoothly, but on production systems you have to frequently provide a new token.
 *
 * The version of the OpenAPI document: 9.0.3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import type {
    AktionType,
    EgfaModuleStatusType,
    EgfaModuleType,
    RolleLokalType,
} from './';

/**
 * @export
 * @interface EgfaModuleDisabilityEntityDTOAllOf
 */
export interface EgfaModuleDisabilityEntityDTOAllOf {
    /**
     * @type {string}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    readonly egfaId?: string;
    /**
     * @type {string}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    title?: string;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    influenceExpected?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    deleted?: boolean;
    /**
     * @type {string}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    influenceSummary?: string;
    /**
     * @type {EgfaModuleStatusType}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    status?: EgfaModuleStatusType;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    readonly allowedToModify?: boolean;
    /**
     * @type {RolleLokalType}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    rolleTyp?: RolleLokalType;
    /**
     * @type {Array<AktionType>}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    aktionen?: Array<AktionType>;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    faktorBehinderteMittelbarBetroffen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    faktorBehinderteUnmittelbarBetroffen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    faktorAnhaltspunkteUnterschiedlichBetroffen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    faktorAnhaltspunkteProfitierenNichtGleich?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    faktorAnhaltspunkteBesondereBelastungen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    veraenderungGeistigeundPsychischeFunktionen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    veraenderungMobilitaet?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    veraenderungHautNervenMuskelnSkelett?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    veraenderungSinnesfunktionen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    veraenderungStimmUndSprechfunktionen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    veraenderungSchmerz?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    veraenderungSonstiges?: boolean;
    /**
     * @type {string}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    veraenderungSonstigesSummary?: string;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeFamilieUndPartnerschaftSowieSozialesNetz?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeBildungUndAusbildung?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeErwerbstaetigkeitUndEinkommen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeAlltaeglicheLebensfuehrung?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeFortbewegungMobilitaet?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeGesundheitFreizeitUndSportSowieKulturUndMedien?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeDigitalisierung?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeSicherheitUndSchutzVorGewalt?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabePolitikUndOeffentlichkeit?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeSonstiges?: boolean;
    /**
     * @type {string}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    teilhabeSonstigesSummary?: string;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    zugangProdukteTechnologien?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    zugangUmwelt?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    zugangUnterstuetzung?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    zugangSystemeDienstleistungen?: boolean;
    /**
     * @type {boolean}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    zugangSonstiges?: boolean;
    /**
     * @type {string}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    zugangSonstigesSummary?: string;
    /**
     * @type {string}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    auswirkungenSummary?: string;
    /**
     * @type {string}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    massnahmenSummary?: string;
    /**
     * @type {EgfaModuleType}
     * @memberof EgfaModuleDisabilityEntityDTOAllOf
     */
    typ?: EgfaModuleType;
}


