// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// tslint:disable
/**
 * eGesetzgebung Platform
 * This is the rest interface offered by the eGesetzgebung Platform backend.  Please provide __JW Access Token__ using button \'Authorize\' to access protected endpoints. Use the following instructions to get token:  1. on CMD/Bash/Terminal: ```curl -d \"client_id=user-management\" -d \"client_secret=INSERT_IAM_SECRET\" -d \"grant_type=password&username=INSERT_USER_EMAIL&password=INSERT_USER_PASSWORD\" \"INSERT_IAM_URL/realms/bund/protocol/openid-connect/token\"``` 2. copy access_token (without quotes, you can use jq or other tools to extract this value automatically) 3. insert value into \'JWToken\' field under \'Authorize\' and press \'Authorize\' 4. now you should have access to user protected endpoint  __FYI:__ token expires after 60 seconds. On non-production systems only a warning is displayed so you can work smoothly, but on production systems you have to frequently provide a new token.
 *
 * The version of the OpenAPI document: 9.0.3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/**
 * 
 * @export
 * @enum {string}
 */
export enum SachgebietType {
    ArbeitUndBeschaeftigung = 'ARBEIT_UND_BESCHAEFTIGUNG',
    AussenpolitikUndInternationaleBeziehungen = 'AUSSENPOLITIK_UND_INTERNATIONALE_BEZIEHUNGEN',
    Aussenwirtschaft = 'AUSSENWIRTSCHAFT',
    BildungUndErziehung = 'BILDUNG_UND_ERZIEHUNG',
    Bundestag = 'BUNDESTAG',
    DeutscheEinheitInnerdeutscheBeziehungen = 'DEUTSCHE_EINHEIT_INNERDEUTSCHE_BEZIEHUNGEN',
    Energie = 'ENERGIE',
    Entwicklungspolitik = 'ENTWICKLUNGSPOLITIK',
    EuropapolitikUndEuropaeischeUnion = 'EUROPAPOLITIK_UND_EUROPAEISCHE_UNION',
    GesellschaftspolitikSozialeGruppen = 'GESELLSCHAFTSPOLITIK_SOZIALE_GRUPPEN',
    Gesundheit = 'GESUNDHEIT',
    InnereSicherheit = 'INNERE_SICHERHEIT',
    Kultur = 'KULTUR',
    LandwirtschaftUndErnaehrung = 'LANDWIRTSCHAFT_UND_ERNAEHRUNG',
    MedienKommunikationUndInformationstechnik = 'MEDIEN_KOMMUNIKATION_UND_INFORMATIONSTECHNIK',
    MigrationAufenthaltsrecht = 'MIGRATION_AUFENTHALTSRECHT',
    OeffentlicheFinanzenSteuernUndAbgaben = 'OEFFENTLICHE_FINANZEN_STEUERN_UND_ABGABEN',
    PolitischesLebenParteien = 'POLITISCHES_LEBEN_PARTEIEN',
    RaumordnungBauUndWohnungswesen = 'RAUMORDNUNG_BAU_UND_WOHNUNGSWESEN',
    Recht = 'RECHT',
    SozialeSicherung = 'SOZIALE_SICHERUNG',
    SportFreizeitUndTourismus = 'SPORT_FREIZEIT_UND_TOURISMUS',
    StaatUndVerwaltung = 'STAAT_UND_VERWALTUNG',
    Umwelt = 'UMWELT',
    Verkehr = 'VERKEHR',
    Verteidigung = 'VERTEIDIGUNG',
    Wirtschaft = 'WIRTSCHAFT',
    WissenschaftForschungUndTechnologie = 'WISSENSCHAFT_FORSCHUNG_UND_TECHNOLOGIE'
}

