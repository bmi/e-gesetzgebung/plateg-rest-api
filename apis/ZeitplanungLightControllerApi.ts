// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// tslint:disable
/**
 * eGesetzgebung Platform
 * This is the rest interface offered by the eGesetzgebung Platform backend.  Please provide __JW Access Token__ using button \'Authorize\' to access protected endpoints. Use the following instructions to get token:  1. on CMD/Bash/Terminal: ```curl -d \"client_id=user-management\" -d \"client_secret=INSERT_IAM_SECRET\" -d \"grant_type=password&username=INSERT_USER_EMAIL&password=INSERT_USER_PASSWORD\" \"INSERT_IAM_URL/realms/bund/protocol/openid-connect/token\"``` 2. copy access_token (without quotes, you can use jq or other tools to extract this value automatically) 3. insert value into \'JWToken\' field under \'Authorize\' and press \'Authorize\' 4. now you should have access to user protected endpoint  __FYI:__ token expires after 60 seconds. On non-production systems only a warning is displayed so you can work smoothly, but on production systems you have to frequently provide a new token.
 *
 * The version of the OpenAPI document: 9.0.3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import type { Observable } from 'rxjs';
import type { AjaxResponse } from 'rxjs/ajax';
import { BaseAPI, throwIfNullOrUndefined, encodeURI } from '../runtime';
import type { OperationOpts, HttpHeaders } from '../runtime';
import type {
    GetKalender200ResponseInner,
    ZeitplanungCreateFromVorlageRequestDTO,
    ZeitplanungCreateFromZeitplanungRequestDTO,
    ZeitplanungEntityDTO,
    ZeitplanungEntityResponseDTO,
    ZeitplanungPhaseRequest,
    ZeitplanungTerminRequest,
    ZeitplanungsvorlageEntityResponseDTO,
} from '../models';

export interface AddDateToZeitplanungLightRequest {
    zeitplanungTerminRequest: ZeitplanungTerminRequest;
}

export interface AddPhaseToZeitplanungLightRequest {
    zeitplanungPhaseRequest: ZeitplanungPhaseRequest;
}

export interface CreateZeitplanungLightRequest {
    zeitplanungCreateFromVorlageRequestDTO: ZeitplanungCreateFromVorlageRequestDTO;
}

export interface CreateZeitplanungsvorlageLight1Request {
    zeitplanungCreateFromZeitplanungRequestDTO: ZeitplanungCreateFromZeitplanungRequestDTO;
}

export interface DeleteZeitplanungselementFromZeitplanungLightRequest {
    elementId: string;
    zeitplanungAnpassen: boolean;
    zeitplanungEntityDTO: ZeitplanungEntityDTO;
}

export interface ModifyDateInZeitplanungLightRequest {
    elementId: string;
    zeitplanungTerminRequest: ZeitplanungTerminRequest;
}

export interface ModifyPhaseInZeitplanungLightRequest {
    elementId: string;
    zeitplanungPhaseRequest: ZeitplanungPhaseRequest;
}

/**
 * no description
 */
export class ZeitplanungLightControllerApi extends BaseAPI {

    /**
     * Fügt einer Zeitplanung einen neuen Termin hinzu
     * POST ZEITPLANUNG DATE
     */
    addDateToZeitplanungLight({ zeitplanungTerminRequest }: AddDateToZeitplanungLightRequest): Observable<ZeitplanungEntityResponseDTO>
    addDateToZeitplanungLight({ zeitplanungTerminRequest }: AddDateToZeitplanungLightRequest, opts?: OperationOpts): Observable<AjaxResponse<ZeitplanungEntityResponseDTO>>
    addDateToZeitplanungLight({ zeitplanungTerminRequest }: AddDateToZeitplanungLightRequest, opts?: OperationOpts): Observable<ZeitplanungEntityResponseDTO | AjaxResponse<ZeitplanungEntityResponseDTO>> {
        throwIfNullOrUndefined(zeitplanungTerminRequest, 'zeitplanungTerminRequest', 'addDateToZeitplanungLight');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<ZeitplanungEntityResponseDTO>({
            url: '/zeitplanung-light/date',
            method: 'POST',
            headers,
            body: zeitplanungTerminRequest,
        }, opts?.responseOpts);
    };

    /**
     * Fügt einer Zeitplanung eine neue Phase hinzu
     * POST ZEITPLANUNG PHASE
     */
    addPhaseToZeitplanungLight({ zeitplanungPhaseRequest }: AddPhaseToZeitplanungLightRequest): Observable<ZeitplanungEntityResponseDTO>
    addPhaseToZeitplanungLight({ zeitplanungPhaseRequest }: AddPhaseToZeitplanungLightRequest, opts?: OperationOpts): Observable<AjaxResponse<ZeitplanungEntityResponseDTO>>
    addPhaseToZeitplanungLight({ zeitplanungPhaseRequest }: AddPhaseToZeitplanungLightRequest, opts?: OperationOpts): Observable<ZeitplanungEntityResponseDTO | AjaxResponse<ZeitplanungEntityResponseDTO>> {
        throwIfNullOrUndefined(zeitplanungPhaseRequest, 'zeitplanungPhaseRequest', 'addPhaseToZeitplanungLight');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<ZeitplanungEntityResponseDTO>({
            url: '/zeitplanung-light/phase',
            method: 'POST',
            headers,
            body: zeitplanungPhaseRequest,
        }, opts?.responseOpts);
    };

    /**
     * Legt eine neue Zeitplanung an
     * POST ZEITPLANUNG
     */
    createZeitplanungLight({ zeitplanungCreateFromVorlageRequestDTO }: CreateZeitplanungLightRequest): Observable<ZeitplanungEntityResponseDTO>
    createZeitplanungLight({ zeitplanungCreateFromVorlageRequestDTO }: CreateZeitplanungLightRequest, opts?: OperationOpts): Observable<AjaxResponse<ZeitplanungEntityResponseDTO>>
    createZeitplanungLight({ zeitplanungCreateFromVorlageRequestDTO }: CreateZeitplanungLightRequest, opts?: OperationOpts): Observable<ZeitplanungEntityResponseDTO | AjaxResponse<ZeitplanungEntityResponseDTO>> {
        throwIfNullOrUndefined(zeitplanungCreateFromVorlageRequestDTO, 'zeitplanungCreateFromVorlageRequestDTO', 'createZeitplanungLight');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<ZeitplanungEntityResponseDTO>({
            url: '/zeitplanung-light',
            method: 'POST',
            headers,
            body: zeitplanungCreateFromVorlageRequestDTO,
        }, opts?.responseOpts);
    };

    /**
     * Legt eine neue Zeitplanungsvorlage an
     * POST ZEITPLANUNGSVORLAGE
     */
    createZeitplanungsvorlageLight1({ zeitplanungCreateFromZeitplanungRequestDTO }: CreateZeitplanungsvorlageLight1Request): Observable<ZeitplanungsvorlageEntityResponseDTO>
    createZeitplanungsvorlageLight1({ zeitplanungCreateFromZeitplanungRequestDTO }: CreateZeitplanungsvorlageLight1Request, opts?: OperationOpts): Observable<AjaxResponse<ZeitplanungsvorlageEntityResponseDTO>>
    createZeitplanungsvorlageLight1({ zeitplanungCreateFromZeitplanungRequestDTO }: CreateZeitplanungsvorlageLight1Request, opts?: OperationOpts): Observable<ZeitplanungsvorlageEntityResponseDTO | AjaxResponse<ZeitplanungsvorlageEntityResponseDTO>> {
        throwIfNullOrUndefined(zeitplanungCreateFromZeitplanungRequestDTO, 'zeitplanungCreateFromZeitplanungRequestDTO', 'createZeitplanungsvorlageLight1');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<ZeitplanungsvorlageEntityResponseDTO>({
            url: '/zeitplanung-light/zeitplanungsvorlage',
            method: 'POST',
            headers,
            body: zeitplanungCreateFromZeitplanungRequestDTO,
        }, opts?.responseOpts);
    };

    /**
     * Löscht eine Phase oder einen Termin aus der Zeitplanung
     * DELETE ZEITPLANUNG PHASE/DATE
     */
    deleteZeitplanungselementFromZeitplanungLight({ elementId, zeitplanungAnpassen, zeitplanungEntityDTO }: DeleteZeitplanungselementFromZeitplanungLightRequest): Observable<ZeitplanungEntityResponseDTO>
    deleteZeitplanungselementFromZeitplanungLight({ elementId, zeitplanungAnpassen, zeitplanungEntityDTO }: DeleteZeitplanungselementFromZeitplanungLightRequest, opts?: OperationOpts): Observable<AjaxResponse<ZeitplanungEntityResponseDTO>>
    deleteZeitplanungselementFromZeitplanungLight({ elementId, zeitplanungAnpassen, zeitplanungEntityDTO }: DeleteZeitplanungselementFromZeitplanungLightRequest, opts?: OperationOpts): Observable<ZeitplanungEntityResponseDTO | AjaxResponse<ZeitplanungEntityResponseDTO>> {
        throwIfNullOrUndefined(elementId, 'elementId', 'deleteZeitplanungselementFromZeitplanungLight');
        throwIfNullOrUndefined(zeitplanungAnpassen, 'zeitplanungAnpassen', 'deleteZeitplanungselementFromZeitplanungLight');
        throwIfNullOrUndefined(zeitplanungEntityDTO, 'zeitplanungEntityDTO', 'deleteZeitplanungselementFromZeitplanungLight');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<ZeitplanungEntityResponseDTO>({
            url: '/zeitplanung-light/element/{elementId}/{zeitplanungAnpassen}'.replace('{elementId}', encodeURI(elementId)).replace('{zeitplanungAnpassen}', encodeURI(zeitplanungAnpassen)),
            method: 'DELETE',
            headers,
            body: zeitplanungEntityDTO,
        }, opts?.responseOpts);
    };

    /**
     * Ruft die Kalender ab
     * GET KALENDER LIST
     */
    getKalenderLight(): Observable<Array<GetKalender200ResponseInner>>
    getKalenderLight(opts?: OperationOpts): Observable<AjaxResponse<Array<GetKalender200ResponseInner>>>
    getKalenderLight(opts?: OperationOpts): Observable<Array<GetKalender200ResponseInner> | AjaxResponse<Array<GetKalender200ResponseInner>>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<Array<GetKalender200ResponseInner>>({
            url: '/zeitplanung-light/kalender',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Ändert einen Termin
     * PUT ZEITPLANUNG DATE
     */
    modifyDateInZeitplanungLight({ elementId, zeitplanungTerminRequest }: ModifyDateInZeitplanungLightRequest): Observable<ZeitplanungEntityResponseDTO>
    modifyDateInZeitplanungLight({ elementId, zeitplanungTerminRequest }: ModifyDateInZeitplanungLightRequest, opts?: OperationOpts): Observable<AjaxResponse<ZeitplanungEntityResponseDTO>>
    modifyDateInZeitplanungLight({ elementId, zeitplanungTerminRequest }: ModifyDateInZeitplanungLightRequest, opts?: OperationOpts): Observable<ZeitplanungEntityResponseDTO | AjaxResponse<ZeitplanungEntityResponseDTO>> {
        throwIfNullOrUndefined(elementId, 'elementId', 'modifyDateInZeitplanungLight');
        throwIfNullOrUndefined(zeitplanungTerminRequest, 'zeitplanungTerminRequest', 'modifyDateInZeitplanungLight');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<ZeitplanungEntityResponseDTO>({
            url: '/zeitplanung-light/date/{elementId}'.replace('{elementId}', encodeURI(elementId)),
            method: 'PUT',
            headers,
            body: zeitplanungTerminRequest,
        }, opts?.responseOpts);
    };

    /**
     * Ändert eine Phase
     * PUT ZEITPLANUNG PHASE
     */
    modifyPhaseInZeitplanungLight({ elementId, zeitplanungPhaseRequest }: ModifyPhaseInZeitplanungLightRequest): Observable<ZeitplanungEntityResponseDTO>
    modifyPhaseInZeitplanungLight({ elementId, zeitplanungPhaseRequest }: ModifyPhaseInZeitplanungLightRequest, opts?: OperationOpts): Observable<AjaxResponse<ZeitplanungEntityResponseDTO>>
    modifyPhaseInZeitplanungLight({ elementId, zeitplanungPhaseRequest }: ModifyPhaseInZeitplanungLightRequest, opts?: OperationOpts): Observable<ZeitplanungEntityResponseDTO | AjaxResponse<ZeitplanungEntityResponseDTO>> {
        throwIfNullOrUndefined(elementId, 'elementId', 'modifyPhaseInZeitplanungLight');
        throwIfNullOrUndefined(zeitplanungPhaseRequest, 'zeitplanungPhaseRequest', 'modifyPhaseInZeitplanungLight');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<ZeitplanungEntityResponseDTO>({
            url: '/zeitplanung-light/phase/{elementId}'.replace('{elementId}', encodeURI(elementId)),
            method: 'PUT',
            headers,
            body: zeitplanungPhaseRequest,
        }, opts?.responseOpts);
    };

}
