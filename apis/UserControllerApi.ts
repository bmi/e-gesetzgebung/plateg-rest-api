// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// tslint:disable
/**
 * eGesetzgebung Platform
 * This is the rest interface offered by the eGesetzgebung Platform backend.  Please provide __JW Access Token__ using button \'Authorize\' to access protected endpoints. Use the following instructions to get token:  1. on CMD/Bash/Terminal: ```curl -d \"client_id=user-management\" -d \"client_secret=INSERT_IAM_SECRET\" -d \"grant_type=password&username=INSERT_USER_EMAIL&password=INSERT_USER_PASSWORD\" \"INSERT_IAM_URL/realms/bund/protocol/openid-connect/token\"``` 2. copy access_token (without quotes, you can use jq or other tools to extract this value automatically) 3. insert value into \'JWToken\' field under \'Authorize\' and press \'Authorize\' 4. now you should have access to user protected endpoint  __FYI:__ token expires after 60 seconds. On non-production systems only a warning is displayed so you can work smoothly, but on production systems you have to frequently provide a new token.
 *
 * The version of the OpenAPI document: 9.0.3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import type { Observable } from 'rxjs';
import type { AjaxResponse } from 'rxjs/ajax';
import { BaseAPI, throwIfNullOrUndefined, encodeURI } from '../runtime';
import type { OperationOpts, HttpHeaders, HttpQuery } from '../runtime';
import type {
    HomepageTableDTO,
    NutzerRollenRequestDTO,
    NutzerRollenResponseDTO,
    UserEinstellungEntityDTO,
    UserEntityListResponseShortDTO,
    UserEntityResponseDTO,
    UserEntityWithStellvertreterResponseDTO,
    UserSearchShortResult,
} from '../models';

export interface DeleteRoleRequest {
    email: string;
}

export interface DeleteRoleByRessortRequest {
    email: string;
}

export interface GetUserShortListByEmailsRequest {
    emails: Array<string>;
}

export interface GetUserShortListBySearchStringsRequest {
    suchStrings: Array<string>;
    ressortId?: string;
    referatId?: string;
}

export interface GetUserShortListBySearchStringsForBundesratRequest {
    suchStrings: Array<string>;
}

export interface GetUserShortListBySearchStringsForBundestagRequest {
    suchStrings: Array<string>;
}

export interface GetUserWithStellvertreterRequest {
    userId: string;
}

export interface SaveUsersWithRolesRequest {
    nutzerRollenRequestDTO: NutzerRollenRequestDTO;
}

export interface SaveUsersWithRolesByRessortRequest {
    nutzerRollenRequestDTO: NutzerRollenRequestDTO;
}

export interface SetShowZeitplanungScrollDialogRequest {
    value: boolean;
}

export interface SetShowZeitplanungSystemvorschlagOpenDialogRequest {
    value: boolean;
}

export interface SetStellvertreterRequest {
    requestBody: Array<string>;
}

export interface SetUserSettingsRequest {
    userEinstellungEntityDTO: UserEinstellungEntityDTO;
}

/**
 * no description
 */
export class UserControllerApi extends BaseAPI {

    /**
     * Löscht die Rolle vom Bundestagsnutzer
     * Löscht eine Rolle
     */
    deleteRole({ email }: DeleteRoleRequest): Observable<void>
    deleteRole({ email }: DeleteRoleRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    deleteRole({ email }: DeleteRoleRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        throwIfNullOrUndefined(email, 'email', 'deleteRole');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/bundestag/role/user/{email}'.replace('{email}', encodeURI(email)),
            method: 'DELETE',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Löscht die Rolle vom Nutzer des eigenen Ressorts
     * Löscht eine Rolle
     */
    deleteRoleByRessort({ email }: DeleteRoleByRessortRequest): Observable<void>
    deleteRoleByRessort({ email }: DeleteRoleByRessortRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    deleteRoleByRessort({ email }: DeleteRoleByRessortRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        throwIfNullOrUndefined(email, 'email', 'deleteRoleByRessort');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/role/user/{email}'.replace('{email}', encodeURI(email)),
            method: 'DELETE',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Lädt alle Bundestagsnutzer mit Rollen
     * Lädt die Nutzer.
     */
    getAllBTUsersWithRoles(): Observable<NutzerRollenResponseDTO>
    getAllBTUsersWithRoles(opts?: OperationOpts): Observable<AjaxResponse<NutzerRollenResponseDTO>>
    getAllBTUsersWithRoles(opts?: OperationOpts): Observable<NutzerRollenResponseDTO | AjaxResponse<NutzerRollenResponseDTO>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<NutzerRollenResponseDTO>({
            url: '/user/bundestag/role/',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Lädt alle Nutzer des eigenen Ressorts mit Rollen
     * Lädt die Nutzer.
     */
    getAllUsersWithRolesByRessort(): Observable<NutzerRollenResponseDTO>
    getAllUsersWithRolesByRessort(opts?: OperationOpts): Observable<AjaxResponse<NutzerRollenResponseDTO>>
    getAllUsersWithRolesByRessort(opts?: OperationOpts): Observable<NutzerRollenResponseDTO | AjaxResponse<NutzerRollenResponseDTO>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<NutzerRollenResponseDTO>({
            url: '/user/role/',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Ruft die Tabelle für die Startseite
     * Startseiten-Tabelle
     */
    getHomepageTable(): Observable<Array<HomepageTableDTO>>
    getHomepageTable(opts?: OperationOpts): Observable<AjaxResponse<Array<HomepageTableDTO>>>
    getHomepageTable(opts?: OperationOpts): Observable<Array<HomepageTableDTO> | AjaxResponse<Array<HomepageTableDTO>>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<Array<HomepageTableDTO>>({
            url: '/user/homepage/table',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Lädt die Liste meiner Stellvertreter (Personen die mich stellvertreten)
     * Stellvertreter laden
     */
    getStellvertreter(): Observable<Array<UserEntityResponseDTO>>
    getStellvertreter(opts?: OperationOpts): Observable<AjaxResponse<Array<UserEntityResponseDTO>>>
    getStellvertreter(opts?: OperationOpts): Observable<Array<UserEntityResponseDTO> | AjaxResponse<Array<UserEntityResponseDTO>>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<Array<UserEntityResponseDTO>>({
            url: '/user/stellvertreter',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Lädt die Liste meiner Stellvertretungen (Personen die ich stellvertrete)
     * Stellvertretungen laden
     */
    getStellvertretungen(): Observable<Array<UserEntityResponseDTO>>
    getStellvertretungen(opts?: OperationOpts): Observable<AjaxResponse<Array<UserEntityResponseDTO>>>
    getStellvertretungen(opts?: OperationOpts): Observable<Array<UserEntityResponseDTO> | AjaxResponse<Array<UserEntityResponseDTO>>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<Array<UserEntityResponseDTO>>({
            url: '/user/stellvertretungen',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Lädt den aktuellen User
     * GET CURRENT USER
     */
    getUser(): Observable<UserEntityWithStellvertreterResponseDTO>
    getUser(opts?: OperationOpts): Observable<AjaxResponse<UserEntityWithStellvertreterResponseDTO>>
    getUser(opts?: OperationOpts): Observable<UserEntityWithStellvertreterResponseDTO | AjaxResponse<UserEntityWithStellvertreterResponseDTO>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UserEntityWithStellvertreterResponseDTO>({
            url: '/user',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Lädt die Nutzereinstellungen
     * Nutzereinstellungen geladen
     */
    getUserEinstellung(): Observable<UserEinstellungEntityDTO>
    getUserEinstellung(opts?: OperationOpts): Observable<AjaxResponse<UserEinstellungEntityDTO>>
    getUserEinstellung(opts?: OperationOpts): Observable<UserEinstellungEntityDTO | AjaxResponse<UserEinstellungEntityDTO>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UserEinstellungEntityDTO>({
            url: '/user/settings',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Lädt die User mit den angegebenen E-Mail-Adressen
     * GET USER BY EMAILS
     */
    getUserShortListByEmails({ emails }: GetUserShortListByEmailsRequest): Observable<UserEntityListResponseShortDTO>
    getUserShortListByEmails({ emails }: GetUserShortListByEmailsRequest, opts?: OperationOpts): Observable<AjaxResponse<UserEntityListResponseShortDTO>>
    getUserShortListByEmails({ emails }: GetUserShortListByEmailsRequest, opts?: OperationOpts): Observable<UserEntityListResponseShortDTO | AjaxResponse<UserEntityListResponseShortDTO>> {
        throwIfNullOrUndefined(emails, 'emails', 'getUserShortListByEmails');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        const query: HttpQuery = { // required parameters are used directly since they are already checked by throwIfNullOrUndefined
            'emails': emails,
        };

        return this.request<UserEntityListResponseShortDTO>({
            url: '/user/short/find/email',
            method: 'GET',
            headers,
            query,
        }, opts?.responseOpts);
    };

    /**
     * Sucht nach Usern in PKP
     * Durchsucht User.
     */
    getUserShortListBySearchStrings({ suchStrings, ressortId, referatId }: GetUserShortListBySearchStringsRequest): Observable<UserSearchShortResult>
    getUserShortListBySearchStrings({ suchStrings, ressortId, referatId }: GetUserShortListBySearchStringsRequest, opts?: OperationOpts): Observable<AjaxResponse<UserSearchShortResult>>
    getUserShortListBySearchStrings({ suchStrings, ressortId, referatId }: GetUserShortListBySearchStringsRequest, opts?: OperationOpts): Observable<UserSearchShortResult | AjaxResponse<UserSearchShortResult>> {
        throwIfNullOrUndefined(suchStrings, 'suchStrings', 'getUserShortListBySearchStrings');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        const query: HttpQuery = {};

        if (ressortId != null) { query['ressortId'] = ressortId; }
        if (referatId != null) { query['referatId'] = referatId; }

        return this.request<UserSearchShortResult>({
            url: '/user/short/find/string/{suchStrings}'.replace('{suchStrings}', encodeURI(suchStrings)),
            method: 'GET',
            headers,
            query,
        }, opts?.responseOpts);
    };

    /**
     * Sucht nach Usern in PKP
     * Durchsucht User.
     */
    getUserShortListBySearchStringsForBundesrat({ suchStrings }: GetUserShortListBySearchStringsForBundesratRequest): Observable<UserSearchShortResult>
    getUserShortListBySearchStringsForBundesrat({ suchStrings }: GetUserShortListBySearchStringsForBundesratRequest, opts?: OperationOpts): Observable<AjaxResponse<UserSearchShortResult>>
    getUserShortListBySearchStringsForBundesrat({ suchStrings }: GetUserShortListBySearchStringsForBundesratRequest, opts?: OperationOpts): Observable<UserSearchShortResult | AjaxResponse<UserSearchShortResult>> {
        throwIfNullOrUndefined(suchStrings, 'suchStrings', 'getUserShortListBySearchStringsForBundesrat');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UserSearchShortResult>({
            url: '/user/bundesrat/short/find/string/{suchStrings}'.replace('{suchStrings}', encodeURI(suchStrings)),
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Sucht nach Usern in PKP
     * Durchsucht User.
     */
    getUserShortListBySearchStringsForBundestag({ suchStrings }: GetUserShortListBySearchStringsForBundestagRequest): Observable<UserSearchShortResult>
    getUserShortListBySearchStringsForBundestag({ suchStrings }: GetUserShortListBySearchStringsForBundestagRequest, opts?: OperationOpts): Observable<AjaxResponse<UserSearchShortResult>>
    getUserShortListBySearchStringsForBundestag({ suchStrings }: GetUserShortListBySearchStringsForBundestagRequest, opts?: OperationOpts): Observable<UserSearchShortResult | AjaxResponse<UserSearchShortResult>> {
        throwIfNullOrUndefined(suchStrings, 'suchStrings', 'getUserShortListBySearchStringsForBundestag');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UserSearchShortResult>({
            url: '/user/bundestag/short/find/string/{suchStrings}'.replace('{suchStrings}', encodeURI(suchStrings)),
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Wechselt den aktuellen User zu einem Stellvertreter
     * User wechseln
     */
    getUserWithStellvertreter({ userId }: GetUserWithStellvertreterRequest): Observable<UserEntityWithStellvertreterResponseDTO>
    getUserWithStellvertreter({ userId }: GetUserWithStellvertreterRequest, opts?: OperationOpts): Observable<AjaxResponse<UserEntityWithStellvertreterResponseDTO>>
    getUserWithStellvertreter({ userId }: GetUserWithStellvertreterRequest, opts?: OperationOpts): Observable<UserEntityWithStellvertreterResponseDTO | AjaxResponse<UserEntityWithStellvertreterResponseDTO>> {
        throwIfNullOrUndefined(userId, 'userId', 'getUserWithStellvertreter');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UserEntityWithStellvertreterResponseDTO>({
            url: '/user/user/{userId}'.replace('{userId}', encodeURI(userId)),
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Fragt ab, ob der Dialog zum horizontalen Scrollen in der Zeitplanung angezeigt werden soll
     * Zeitplanung Scrolldialog anzeigen
     */
    isShowZeitplanungScrollDialog(): Observable<boolean>
    isShowZeitplanungScrollDialog(opts?: OperationOpts): Observable<AjaxResponse<boolean>>
    isShowZeitplanungScrollDialog(opts?: OperationOpts): Observable<boolean | AjaxResponse<boolean>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<boolean>({
            url: '/user/setting/show-zeitplanung-scroll-dialog',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Fragt ab, ob der Dialog beim Öffnen eines Systemvorschlags (Zeitplanungsvorlagenelement) angezeigt werden soll
     * Zeitplanungvorschlag Öffnendialog anzeigen
     */
    isShowZeitplanungSystemvorschlagOpenDialog(): Observable<boolean>
    isShowZeitplanungSystemvorschlagOpenDialog(opts?: OperationOpts): Observable<AjaxResponse<boolean>>
    isShowZeitplanungSystemvorschlagOpenDialog(opts?: OperationOpts): Observable<boolean | AjaxResponse<boolean>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<boolean>({
            url: '/user/setting/show-zeitplanung-systemvorschlag-open-dialog',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Registriert einen neuen User bzw. leitet zur Registrierungsseite des IAM weiter.
     * Registriert User.
     */
    registerUser(): Observable<void>
    registerUser(opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    registerUser(opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/register',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Entfernt den aktuell ausgewählten Stellvertreter
     * User zurücksetzen
     */
    resetUser(): Observable<UserEntityWithStellvertreterResponseDTO>
    resetUser(opts?: OperationOpts): Observable<AjaxResponse<UserEntityWithStellvertreterResponseDTO>>
    resetUser(opts?: OperationOpts): Observable<UserEntityWithStellvertreterResponseDTO | AjaxResponse<UserEntityWithStellvertreterResponseDTO>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UserEntityWithStellvertreterResponseDTO>({
            url: '/user/user',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Speichert die Liste der Bundestagsnutzer mit Rollen
     * Speichert die Nutzer.
     */
    saveUsersWithRoles({ nutzerRollenRequestDTO }: SaveUsersWithRolesRequest): Observable<void>
    saveUsersWithRoles({ nutzerRollenRequestDTO }: SaveUsersWithRolesRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    saveUsersWithRoles({ nutzerRollenRequestDTO }: SaveUsersWithRolesRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        throwIfNullOrUndefined(nutzerRollenRequestDTO, 'nutzerRollenRequestDTO', 'saveUsersWithRoles');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/bundestag/role/',
            method: 'POST',
            headers,
            body: nutzerRollenRequestDTO,
        }, opts?.responseOpts);
    };

    /**
     * Speichert die Liste der Nutzer des eigenen Ressorts mit Rollen
     * Speichert die Nutzer.
     */
    saveUsersWithRolesByRessort({ nutzerRollenRequestDTO }: SaveUsersWithRolesByRessortRequest): Observable<void>
    saveUsersWithRolesByRessort({ nutzerRollenRequestDTO }: SaveUsersWithRolesByRessortRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    saveUsersWithRolesByRessort({ nutzerRollenRequestDTO }: SaveUsersWithRolesByRessortRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        throwIfNullOrUndefined(nutzerRollenRequestDTO, 'nutzerRollenRequestDTO', 'saveUsersWithRolesByRessort');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/role/',
            method: 'POST',
            headers,
            body: nutzerRollenRequestDTO,
        }, opts?.responseOpts);
    };

    /**
     * Legt fest, ob der Dialog zum horizontalen Scrollen in der Zeitplanung angezeigt werden soll
     * Zeitplanung Scrolldialog anzeigen
     */
    setShowZeitplanungScrollDialog({ value }: SetShowZeitplanungScrollDialogRequest): Observable<void>
    setShowZeitplanungScrollDialog({ value }: SetShowZeitplanungScrollDialogRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    setShowZeitplanungScrollDialog({ value }: SetShowZeitplanungScrollDialogRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        throwIfNullOrUndefined(value, 'value', 'setShowZeitplanungScrollDialog');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/setting/show-zeitplanung-scroll-dialog/{value}'.replace('{value}', encodeURI(value)),
            method: 'PUT',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Legt fest, ob der Dialog beim Öffnen eines Systemvorschlags (Zeitplanungsvorlagenelement) angezeigt werden soll
     * Zeitplanungvorschlag Öffnendialog anzeigen
     */
    setShowZeitplanungSystemvorschlagOpenDialog({ value }: SetShowZeitplanungSystemvorschlagOpenDialogRequest): Observable<void>
    setShowZeitplanungSystemvorschlagOpenDialog({ value }: SetShowZeitplanungSystemvorschlagOpenDialogRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    setShowZeitplanungSystemvorschlagOpenDialog({ value }: SetShowZeitplanungSystemvorschlagOpenDialogRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        throwIfNullOrUndefined(value, 'value', 'setShowZeitplanungSystemvorschlagOpenDialog');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/setting/show-zeitplanung-systemvorschlag-open-dialog/{value}'.replace('{value}', encodeURI(value)),
            method: 'PUT',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Legt die Liste der Stellvertreter fest
     * Stellvertreter ernennen oder entfernen
     */
    setStellvertreter({ requestBody }: SetStellvertreterRequest): Observable<void>
    setStellvertreter({ requestBody }: SetStellvertreterRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    setStellvertreter({ requestBody }: SetStellvertreterRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        throwIfNullOrUndefined(requestBody, 'requestBody', 'setStellvertreter');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/stellvertreter',
            method: 'PUT',
            headers,
            body: requestBody,
        }, opts?.responseOpts);
    };

    /**
     * Aktualisiert die Nutzereinstellungen
     * Nutzereinstellungen gespeichert
     */
    setUserSettings({ userEinstellungEntityDTO }: SetUserSettingsRequest): Observable<void>
    setUserSettings({ userEinstellungEntityDTO }: SetUserSettingsRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>>
    setUserSettings({ userEinstellungEntityDTO }: SetUserSettingsRequest, opts?: OperationOpts): Observable<void | AjaxResponse<void>> {
        throwIfNullOrUndefined(userEinstellungEntityDTO, 'userEinstellungEntityDTO', 'setUserSettings');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<void>({
            url: '/user/settings',
            method: 'PUT',
            headers,
            body: userEinstellungEntityDTO,
        }, opts?.responseOpts);
    };

}
