// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

// tslint:disable
/**
 * eGesetzgebung Platform
 * This is the rest interface offered by the eGesetzgebung Platform backend.  Please provide __JW Access Token__ using button \'Authorize\' to access protected endpoints. Use the following instructions to get token:  1. on CMD/Bash/Terminal: ```curl -d \"client_id=user-management\" -d \"client_secret=INSERT_IAM_SECRET\" -d \"grant_type=password&username=INSERT_USER_EMAIL&password=INSERT_USER_PASSWORD\" \"INSERT_IAM_URL/realms/bund/protocol/openid-connect/token\"``` 2. copy access_token (without quotes, you can use jq or other tools to extract this value automatically) 3. insert value into \'JWToken\' field under \'Authorize\' and press \'Authorize\' 4. now you should have access to user protected endpoint  __FYI:__ token expires after 60 seconds. On non-production systems only a warning is displayed so you can work smoothly, but on production systems you have to frequently provide a new token.
 *
 * The version of the OpenAPI document: 9.0.3
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import type { Observable } from 'rxjs';
import type { AjaxResponse } from 'rxjs/ajax';
import { BaseAPI, throwIfNullOrUndefined, encodeURI } from '../runtime';
import type { OperationOpts, HttpHeaders, HttpQuery } from '../runtime';
import type {
    AusschussTypeDTO,
    PaginierungDTO,
    UmlaufEntityRequestDTO,
    UmlaufEntityResponseDTO,
    UmlaufStatusType,
    UmlaufTableDTOs,
} from '../models';

export interface CreateUmlaufRequest {
    umlaufEntityRequestDTO: UmlaufEntityRequestDTO;
}

export interface CreateUmlaufPutRequest {
    umlaufEntityRequestDTO: UmlaufEntityRequestDTO;
}

export interface GetUmlaeufeRequest {
    paginierungDTO: PaginierungDTO;
}

export interface GetUmlaeufeGETRequest {
    dRUCKSACHENUMMER?: string;
    uMLAUFSTATUS?: UmlaufStatusType;
    page?: number;
    size?: number;
    sort?: Array<string>;
}

export interface GetUmlaufRequest {
    id: string;
}

export interface GetUmlaufUmlaeufeIdRequest {
    id: string;
}

export interface UpdateUmlaufRequest {
    id: string;
    umlaufEntityRequestDTO: UmlaufEntityRequestDTO;
}

/**
 * no description
 */
export class UmlaufControllerApi extends BaseAPI {

    /**
     * Erstellt einen neuen Umlauf
     * Umlauf erstellen
     */
    createUmlauf({ umlaufEntityRequestDTO }: CreateUmlaufRequest): Observable<UmlaufEntityResponseDTO>
    createUmlauf({ umlaufEntityRequestDTO }: CreateUmlaufRequest, opts?: OperationOpts): Observable<AjaxResponse<UmlaufEntityResponseDTO>>
    createUmlauf({ umlaufEntityRequestDTO }: CreateUmlaufRequest, opts?: OperationOpts): Observable<UmlaufEntityResponseDTO | AjaxResponse<UmlaufEntityResponseDTO>> {
        throwIfNullOrUndefined(umlaufEntityRequestDTO, 'umlaufEntityRequestDTO', 'createUmlauf');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UmlaufEntityResponseDTO>({
            url: '/umlauf',
            method: 'POST',
            headers,
            body: umlaufEntityRequestDTO,
        }, opts?.responseOpts);
    };

    /**
     * Erstellt einen neuen Umlauf
     * Umlauf erstellen
     */
    createUmlaufPut({ umlaufEntityRequestDTO }: CreateUmlaufPutRequest): Observable<UmlaufEntityResponseDTO>
    createUmlaufPut({ umlaufEntityRequestDTO }: CreateUmlaufPutRequest, opts?: OperationOpts): Observable<AjaxResponse<UmlaufEntityResponseDTO>>
    createUmlaufPut({ umlaufEntityRequestDTO }: CreateUmlaufPutRequest, opts?: OperationOpts): Observable<UmlaufEntityResponseDTO | AjaxResponse<UmlaufEntityResponseDTO>> {
        throwIfNullOrUndefined(umlaufEntityRequestDTO, 'umlaufEntityRequestDTO', 'createUmlaufPut');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UmlaufEntityResponseDTO>({
            url: '/umlauf',
            method: 'PUT',
            headers,
            body: umlaufEntityRequestDTO,
        }, opts?.responseOpts);
    };

    /**
     * Liefert eine konstante Liste der Ausschüsse zurück
     * Liefert Elemente des Enums `AusschussType`.
     */
    getAusschussList(): Observable<Array<AusschussTypeDTO>>
    getAusschussList(opts?: OperationOpts): Observable<AjaxResponse<Array<AusschussTypeDTO>>>
    getAusschussList(opts?: OperationOpts): Observable<Array<AusschussTypeDTO> | AjaxResponse<Array<AusschussTypeDTO>>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<Array<AusschussTypeDTO>>({
            url: '/umlauf/ausschuss',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Liefert alle Umläufe vom aktuellen Nutzer
     * Liefert Umläufe
     */
    getUmlaeufe({ paginierungDTO }: GetUmlaeufeRequest): Observable<UmlaufTableDTOs>
    getUmlaeufe({ paginierungDTO }: GetUmlaeufeRequest, opts?: OperationOpts): Observable<AjaxResponse<UmlaufTableDTOs>>
    getUmlaeufe({ paginierungDTO }: GetUmlaeufeRequest, opts?: OperationOpts): Observable<UmlaufTableDTOs | AjaxResponse<UmlaufTableDTOs>> {
        throwIfNullOrUndefined(paginierungDTO, 'paginierungDTO', 'getUmlaeufe');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UmlaufTableDTOs>({
            url: '/umlauf/umlaeufe',
            method: 'POST',
            headers,
            body: paginierungDTO,
        }, opts?.responseOpts);
    };

    /**
     * Liefert alle Umläufe vom aktuellen Nutzer.
     * Liefert alle Umläufe.
     */
    getUmlaeufeGET({ dRUCKSACHENUMMER, uMLAUFSTATUS, page, size, sort }: GetUmlaeufeGETRequest): Observable<UmlaufTableDTOs>
    getUmlaeufeGET({ dRUCKSACHENUMMER, uMLAUFSTATUS, page, size, sort }: GetUmlaeufeGETRequest, opts?: OperationOpts): Observable<AjaxResponse<UmlaufTableDTOs>>
    getUmlaeufeGET({ dRUCKSACHENUMMER, uMLAUFSTATUS, page, size, sort }: GetUmlaeufeGETRequest, opts?: OperationOpts): Observable<UmlaufTableDTOs | AjaxResponse<UmlaufTableDTOs>> {

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        const query: HttpQuery = {};

        if (dRUCKSACHENUMMER != null) { query['DRUCKSACHENUMMER'] = dRUCKSACHENUMMER; }
        if (uMLAUFSTATUS != null) { query['UMLAUFSTATUS'] = uMLAUFSTATUS; }
        if (page != null) { query['page'] = page; }
        if (size != null) { query['size'] = size; }
        if (sort != null) { query['sort'] = sort; }

        return this.request<UmlaufTableDTOs>({
            url: '/umlauf',
            method: 'GET',
            headers,
            query,
        }, opts?.responseOpts);
    };

    /**
     * Liefert einen spezifischen Umlauf anhand der ID
     * Liefert einen Umlauf
     */
    getUmlauf({ id }: GetUmlaufRequest): Observable<UmlaufEntityResponseDTO>
    getUmlauf({ id }: GetUmlaufRequest, opts?: OperationOpts): Observable<AjaxResponse<UmlaufEntityResponseDTO>>
    getUmlauf({ id }: GetUmlaufRequest, opts?: OperationOpts): Observable<UmlaufEntityResponseDTO | AjaxResponse<UmlaufEntityResponseDTO>> {
        throwIfNullOrUndefined(id, 'id', 'getUmlauf');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UmlaufEntityResponseDTO>({
            url: '/umlauf/{id}'.replace('{id}', encodeURI(id)),
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Liefert alle Umläufe vom aktuellen Nutzer
     * Liefert Umläufe
     */
    getUmlauf1(): Observable<Array<UmlaufEntityResponseDTO>>
    getUmlauf1(opts?: OperationOpts): Observable<AjaxResponse<Array<UmlaufEntityResponseDTO>>>
    getUmlauf1(opts?: OperationOpts): Observable<Array<UmlaufEntityResponseDTO> | AjaxResponse<Array<UmlaufEntityResponseDTO>>> {
        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<Array<UmlaufEntityResponseDTO>>({
            url: '/umlauf/list',
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Liefert einen spezifischen Umlauf anhand der ID
     * Liefert einen Umlauf
     */
    getUmlaufUmlaeufeId({ id }: GetUmlaufUmlaeufeIdRequest): Observable<UmlaufEntityResponseDTO>
    getUmlaufUmlaeufeId({ id }: GetUmlaufUmlaeufeIdRequest, opts?: OperationOpts): Observable<AjaxResponse<UmlaufEntityResponseDTO>>
    getUmlaufUmlaeufeId({ id }: GetUmlaufUmlaeufeIdRequest, opts?: OperationOpts): Observable<UmlaufEntityResponseDTO | AjaxResponse<UmlaufEntityResponseDTO>> {
        throwIfNullOrUndefined(id, 'id', 'getUmlaufUmlaeufeId');

        const headers: HttpHeaders = {
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UmlaufEntityResponseDTO>({
            url: '/umlauf/umlaeufe/{id}'.replace('{id}', encodeURI(id)),
            method: 'GET',
            headers,
        }, opts?.responseOpts);
    };

    /**
     * Aktualisiert einen bestehenden Umlauf
     * Umlauf aktualisieren
     */
    updateUmlauf({ id, umlaufEntityRequestDTO }: UpdateUmlaufRequest): Observable<UmlaufEntityResponseDTO>
    updateUmlauf({ id, umlaufEntityRequestDTO }: UpdateUmlaufRequest, opts?: OperationOpts): Observable<AjaxResponse<UmlaufEntityResponseDTO>>
    updateUmlauf({ id, umlaufEntityRequestDTO }: UpdateUmlaufRequest, opts?: OperationOpts): Observable<UmlaufEntityResponseDTO | AjaxResponse<UmlaufEntityResponseDTO>> {
        throwIfNullOrUndefined(id, 'id', 'updateUmlauf');
        throwIfNullOrUndefined(umlaufEntityRequestDTO, 'umlaufEntityRequestDTO', 'updateUmlauf');

        const headers: HttpHeaders = {
            'Content-Type': 'application/json',
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
            ...(this.configuration.username != null && this.configuration.password != null ? { Authorization: `Basic ${btoa(this.configuration.username + ':' + this.configuration.password)}` } : undefined),
        };

        return this.request<UmlaufEntityResponseDTO>({
            url: '/umlauf/{id}'.replace('{id}', encodeURI(id)),
            method: 'POST',
            headers,
            body: umlaufEntityRequestDTO,
        }, opts?.responseOpts);
    };

}
